package crypto

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDecodeHex(t *testing.T) {
	testIO := []struct {
		name     string
		in       string
		expected []byte
	}{
		{
			"text in hex",
			"FF",
			[]byte{255},
		},
		{
			"text in hex",
			"49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d",
			[]byte("I'm killing your brain like a poisonous mushroom"),
		},
	}

	for _, tt := range testIO {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.expected, DecodeHex(tt.in))
		})
	}
}
func TestEncodeHex(t *testing.T) {
	testIO := []struct {
		name     string
		in       string
		expected string
	}{
		{
			"text in hex",
			"I'm killing your brain like a poisonous mushroom",
			"49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d",
		},
	}

	for _, tt := range testIO {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.expected, EncodeToHex([]byte(tt.in)))
		})
	}
}

func TestHexToBase64(t *testing.T) {
	testIO := []struct {
		name     string
		in       string
		expected string
	}{
		{
			"simple test with one char",
			"A",
			"QQ==",
		},
		{
			in:       "AB",
			expected: "QUI=",
		},
		{
			in:       "ABC",
			expected: "QUJD",
		},
		{
			"text in hex",
			string(DecodeHex("49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d")),
			"SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t",
		},
	}

	for _, tt := range testIO {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.expected, EncodeToBase64([]byte(tt.in)))
		})
	}
}

func TestFixedXor(t *testing.T) {
	testIO := []struct {
		name     string
		s, s1    []byte
		expected []byte
	}{
		{
			"simple test with one char",
			[]byte{1},
			[]byte{1},
			[]byte{0},
		},
		{
			"hex test",
			DecodeHex("1c0111001f010100061a024b53535009181c"),
			DecodeHex("686974207468652062756c6c277320657965"),
			DecodeHex("746865206b696420646f6e277420706c6179"),
		},
	}

	for _, tt := range testIO {
		t.Run(tt.name, func(t *testing.T) {
			res, _ := FixedXor(tt.s, tt.s1)
			assert.Equal(t, tt.expected, res)
		})
	}
}

func TestBreakSingleXorCipher(t *testing.T) {
	testIO := []struct {
		name     string
		input    string
		expected string
	}{
		{
			"simple test",
			"1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736",
			"Cooking MC's like a pound of bacon",
		},
	}
	for _, tt := range testIO {
		t.Run(tt.name, func(t *testing.T) {
			in := DecodeHex(tt.input)
			s := BreakSingleXorCypher(in)
			//fmt.Println("dec key:", s[0].Byte)
			res := string(SingleByteXor(in, s[0].Byte))
			assert.Equal(t, tt.expected, res)
		})
	}
}

func TestDetectXorCipher(t *testing.T) {
	testIO := []struct {
		name     string
		input    [][]byte
		expected string
	}{
		{
			name:     "simple test",
			input:    ReadFileLines("data4.txt"),
			expected: "Now that the party is jumping\n",
		},
	}
	for _, tt := range testIO {
		t.Run(tt.name, func(t *testing.T) {
			res, _, e := DetectSingleXorCipher(tt.input)
			assert.NoError(t, e)
			assert.Equal(t, tt.expected, res)
		})
	}
}
