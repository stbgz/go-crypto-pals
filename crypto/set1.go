package crypto

import (
	"container/heap"
	"fmt"
	"math"
	"strconv"
	"strings"

	"unicode"
)

const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"

//EncodeToHex encode byte array to hex
func EncodeToHex(input []byte) string {
	res := ""
	for _, c := range input {
		res += fmt.Sprintf("%x", c)
	}
	return res
}

//DecodeHex decode to byte array
func DecodeHex(hex string) []byte {
	var res []byte
	inChan := make(chan rune)
	go func() {
		for _, c := range hex {
			inChan <- c
		}
		close(inChan)
	}()
	for c := range inChan {
		msb := c
		lsb := <-inChan
		num := string(msb) + string(lsb)
		b, _ := strconv.ParseUint(num, 16, 0)
		res = append(res, byte(b))
	}
	return res
}

//EncodeToBase64 encodes byte array to base64
func EncodeToBase64(input []byte) string {
	res := ""
	for i := 0; i < len(input); i += 3 {
		f := (input[i] & 0xFC) >> 2
		res += string(alphabet[f])
		if len(input) <= i+1 {
			f = (input[i] & 0x03) << 4
			res += string(alphabet[f]) + "=="
			return res
		}

		f = (input[i]&0x03)<<4 | (input[i+1]&0xF0)>>4
		res += string(alphabet[f])
		if len(input) <= i+2 {
			f := (input[i+1] & 0xF) << 2
			res += string(alphabet[f]) + "="
			return res
		}
		f = (input[i+1]&0x0F)<<2 | (input[i+2]&0xC0)>>6
		res += string(alphabet[f])
		f = input[i+2] & 0x3F
		res += string(alphabet[f])
	}
	return res
}

//FixedXor xors two buffers
func FixedXor(s, s1 []byte) (res []byte, e error) {
	if len(s) != len(s1) {
		return nil, fmt.Errorf("input buffers should be the same length")
	}

	res = make([]byte, len(s))
	for i := 0; i < len(s); i++ {
		res[i] = s[i] ^ s1[i]
	}
	return res, nil
}

//SingleByteXor xor  buffers with a byte
func SingleByteXor(s []byte, k byte) (res []byte) {
	res = make([]byte, len(s))
	for i := 0; i < len(s); i++ {
		res[i] = s[i] ^ k
	}
	return res
}

//ScoreBufferAsText simple scoring of byte buffer as english text, by looking at
//printable characters
func ScoreBufferAsText(s []byte) float64 {
	noPrintChar := 0
	for _, c := range s {
		if !unicode.IsPrint(rune(c)) {
			noPrintChar++
		}
	}

	bf := CalculateFrequencies(s)
	sum := 0.0
	for c, f := range bf.Bytes {
		cbf := float64(f) / float64(len(s))
		ef := 0.0
		ss := strings.ToLower(string(c))
		if eff, ok := EnglishLetterFreqs[ss[0]]; ok {
			ef = eff
		}
		sum += math.Sqrt(ef * cbf)
	}
	return sum - (0.01 * float64(noPrintChar))
}

func BreakSingleXorCypher(cipher []byte) ByteAndIntegerMaxHeap {
	scores := ByteAndIntegerMaxHeap{}
	for i := 1; i <= 255; i++ {
		res := SingleByteXor(cipher, byte(i))
		val := ScoreBufferAsText(res)
		heap.Push(&scores, ByteAndInt{Byte: byte(i), Count: float64(val)})
	}
	return scores
}

func DetectSingleXorCipher(ciphers [][]byte) (string, int, error) {
	maxRes := struct {
		bestScore float64
		index     int
	}{}
	var res string
	for i, c := range ciphers {
		decoded := DecodeHex(string(c))
		scores := BreakSingleXorCypher(decoded)
		if maxRes.bestScore < scores[0].Count {
			maxRes.bestScore = scores[0].Count
			maxRes.index = i
			res = string(SingleByteXor(decoded, scores[0].Byte))
		}
	}
	//fmt.Println(res)
	return res, maxRes.index, nil
}

//CalculateFrequencies returns a table with each char in the buffer and the frequency of them
//also returns a heap sorted by the frequency
func CalculateFrequencies(s []byte) ByteFrequencies {
	freq := make(map[byte]int)
	h := &ByteAndIntegerMaxHeap{}

	for _, c := range s {
		val := 1
		if _, ok := freq[c]; ok {
			val = freq[c] + 1
		}
		freq[c] = val
	}
	for k, v := range freq {
		heap.Push(h, ByteAndInt{k, float64(v)})
	}
	return ByteFrequencies{Bytes: freq, Heap: *h}
}
